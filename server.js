const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const flash=require("connect-flash");
const cookieSession = require('cookie-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var mongojs=require('mongojs');
const MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var session = require('express-session');
var cors = require('cors')

var url = 'mongodb://admin:admin@ds111279.mlab.com:11279/litmus-users';
var db=mongojs(url); 

const app = express();
var port = process.env.PORT || 3000;
app.use(cors())

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(__dirname+'/public'));
app.use(cookieParser());
app.use(flash());

// app.use(session({ secret: "cats", resave: false, saveUninitialized: true}));
app.use(cookieSession({
  name: 'session',
  keys: ['abcd'],
  // Cookie Options
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))

// Passport init
app.use(passport.initialize());
app.use(passport.session());


passport.use(new LocalStrategy(
     function(username, password, done) {
        //console.log(username, password)
     	//var url = 'mongodb://localhost:27017/litmus-users';
         MongoClient.connect(url, (err, db) => {
             if(err) {
                 throw err;
             } 
             db.collection('managers').find({ username: username}).toArray().then((user) => {
                 console.log(user);
                 if (err) { return done(err); }
                 if (user.length < 1) {
                 	console.log('user not find');
                     return done(null, false, { message: 'Incorrect username.' });
                 }
                 if (user[0].password !== password) {
                 	console.log('password not matched');
                     return done(null, false, { message: 'Incorrect password.' });
                 }

                 console.log('Success');
                return done(null, user[0]);
            
            }, (err) => {
                console.log(err);
                 throw err;
             });
             db.close();
         });
     })
 );


passport.serializeUser(function(user, done) {
    
    console.log('Serialize user:',user);
    done(null, user);
});
  
passport.deserializeUser(function(user, done) {
    console.log("Deserialize called...",user);
    done(null, user);
});


app.post('/api/login',passport.authenticate('local', { 
                            successRedirect: '/api/success',
                           failureRedirect: '/api/failure',
                           failureFlash: true })
                                                
);


app.get('/api/success', (req, res) => {
    //res.sendFile(__dirname+'/public/show_feeds.html');
    //    console.log("req \n\n\n\n\n\n", res.req.user)
    res.status(200).send('Login successful'+JSON.stringify(res.req.user));

    
});

app.get('/api/failure', (req, res) => {
    res.status(401).send('Invalid');
});


app.get('/api/current-user', (req, res) => {
   // console.log("current user called req.user", req.headers);
    res.status(200).send(JSON.stringify(req.user));
        
});

app.get('/api/logout',(req, res) => {
    req.logout();
    res.redirect("/");
});

app.get('/api/show-feeds',(req,res)=> {
    // console.log('/show-feeds ===> login user', req.user)
    if(req.user.access.has_access === "false"){
        res.send('Access Denied');
    }
    console.log(req.user.access.brand_id);
    var brand_id = req.user.access.brand_id
    db.collection('feeds').find({brand_id: brand_id},function(err,doc){
            res.send(doc);
    }); 
});


app.post('/api/user-rating',function(req,res){
    var name=req.body.name;
    var email=req.body.email;
    var country=req.body.country;
    
    var brand_name=req.body.brand_name;
    var rating=req.body.rating;
    var date = new Date();
    var created_date = date.toISOString().slice(0,10);
  // var brand_id = new ObjectID(brand_name._id);

  db.collection('brands').findOne({name: brand_name},function(err,doc){
       var id=doc._id.$oid;
       
         db.collection('feeds').insert({name: name, email: email, country: country ,brand_id: id, rating: rating, created_date: created_date},function(err,doc){
              res.json(doc);

         });
  });
   
           
});


app.listen(port);
console.log('server running on port '+port);

